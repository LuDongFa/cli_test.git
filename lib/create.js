import path from "path";
import fs from "fs-extra";
import inquirer from "inquirer";
import Generator from "./generator.js";

export default async function (targetDir, options) {
  // 判断目录是否已存在 面试题
  if (fs.existsSync(targetDir)) {
    // 是否强制创建目录
    if (options.force) {
      await fs.remove(targetDir);
    } else {
      // 增加反馈，是否需要覆盖
      let { action } = await inquirer.prompt([
        {
          name: "action",
          type: "list",
          message: "目录已存在，请选择：",
          choices: [
            {
              name: "覆盖",
              value: true,
            },
            {
              name: "取消",
              value: false,
            },
          ],
        },
      ]);
      if (action) {
        await fs.remove(targetDir);
        await fs.mkdir(targetDir);
      } else {
        console.log("用户放弃覆盖，程序退出");
        return
      }
    }
  } else {
    fs.mkdir(targetDir);
  }

  const generator = new Generator('', targetDir);
  generator.create();
}

