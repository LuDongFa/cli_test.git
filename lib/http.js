import axios from "axios";
import { githubConfig } from "../config/repoConfig.js";

function getRepoList() {
  return axios.get(githubConfig.reposUrl);
}
function getTagList(repo) {
  return axios.get(`xxxx/reposityName/${repo}/tags`);
}

axios.interceptors.response.use((res) => {
  return res.data;
});
export { getRepoList, getTagList };
