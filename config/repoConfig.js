// github配置信息
export const githubConfig = {
  // 可以给每种模版配置都构建一个仓库，也可以在一个仓库里构建多个模版配置，
  owner: "piggymorning", // git 仓库作者名
  reposUrl: "https://api.github.com/users/piggymorning/repos",

  repoName: "cli_templates", // git 仓库名称
  ref: "master", // git 仓库指定 branch，commit 或 tag，
  relativePath: ".", // 指定git所需要下载的目录或者文件相对位置
  username: "lvjjmouth@sina.com", // 指定git用户名, 在下载私有仓库时需要的配置参数.
  password: "", // 指定git密码, 同username 一起使用，在下载私有仓库时需要的配置参数.
  token: "", // git token 是另一种登录方式的可配置参数，用于下载私有仓库.
};

// gitee配置信息
export const giteeConfig = {
  owner: "LuDongFa", // git 仓库作者名
  reposUrl: "",

  repoName: "", // git 仓库名称
  ref: "master", // git 仓库指定 branch，commit 或 tag，
  relativePath: ".", // 指定git所需要下载的目录或者文件相对位置
  username: "lvjjmouth@sina.com", // 指定git用户名, 在下载私有仓库时需要的配置参数.
  password: "", // 指定git密码, 同username 一起使用，在下载私有仓库时需要的配置参数.
  token: "", // git token 是另一种登录方式的可配置参数，用于下载私有仓库.
};
