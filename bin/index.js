#! /usr/bin/env node
// 第一行的意思是使用node引擎来解析代码,与node所在地址无关
import { program } from "commander";
import chalk from "chalk";
import figlet from "figlet";
import path from "path";
import fs from "fs-extra";
import inquirer from "inquirer";
import create from "../lib/create.js";

program.on("--help", () => {
  console.log(
    "\r\n" +
      chalk.white.bgBlueBright.bold(
        figlet.textSync("nanChengFE", {
          font: "Standard",
          horizontalLayout: "default",
          verticalLayout: "default",
          width: 80,
          whitespaceBreak: true,
        })
      )
  );
  console.log(
    `\r\nRun ${chalk.cyan(
      `fe <command> --help`
    )} for detailed usage of given command\r\n`
  );
});
/* 
  1、添加--force
  2、把参数传递给create
  3、跑通模版下载逻辑
 */
program
  .command("create <name>")
  .description("创建一个新工程")
  .option("-f,--force", "if file already exist,remove it")
  .option("-o,--color <type>", "input a color", "blue")
  .option("-l,--letters [letter...]", "input some letter")
  .action(async (name, options) => {
    let cwd = process.cwd();
    let targetDir = path.join(cwd, name);

    create(targetDir, options);
  });

// 解析用户传入的参数一定要放在最后一行，统一解析
program.parse(process.argv);
